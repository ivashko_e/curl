<?php

/**
 * Interface Universal_Parser_Adapter_Interface
 */
interface Universal_Parser_Adapter_Interface
{
    public function setOptions();

    public function setOption($key, $value);

    public function setUrl($url);

    public function setProxy($proxy);

    public function setCookieFile($cookieFile);

    public function setReferer($referer);

    public function setPost($params = NULL);

    public function setUserAgent($userAgent = NULL);

    public function content();
}