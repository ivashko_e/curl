<?php

/**
 * Class Universal_Parser_Adapter_MultiCurl
 */
class Universal_Parser_Adapter_MultiCurl
{
    private $_cmh;
    private $_tasks;
    private $_curl;
    private $_curlClass;
    private $_getContent = TRUE;
    private $_getInfo    = TRUE;

    public function __construct($options = [])
    {
        $this->_cmh = curl_multi_init();
    }

    /**
     * @param mixed $curl
     */
    public function setCurl($curl)
    {
        $this->_curl = $curl;

        return $this;
    }

    /**
     * @param mixed $curlClass
     */
    public function setCurlClass($curlClass)
    {
        $this->_curlClass = $curlClass;

        return $this;
    }

    /**
     * @param boolean $getContent
     */
    public function setGetContent($getContent = TRUE)
    {
        $this->_getContent = $getContent;

        return $this;
    }

    /**
     * @param boolean $getInfo
     */
    public function setGetInfo($getInfo = TRUE)
    {
        $this->_getInfo = $getInfo;

        return $this;
    }

    /**
     * @param $k
     * @param $task
     * @return $this
     * @throws Exception
     */
    public function setTask($k, $task)
    {
        if (!empty($task['options'])) {

            $this->_curl->setOptions($task['options']);
        }

        if (empty($task['url'])) {

            throw new Exception('Не указана ссылка на ресурс.');
        }

        $this->_curl->setUrl($task['url']);

        $task['ch'] = $this->_curl->getCh();
        $this->_tasks[$k] = $task;

        curl_multi_add_handle($this->_cmh, $this->_curl->getCh());

        return $this;
    }

    /**
     * @param $tasks
     * @return $this
     * @throws Exception
     */
    public function setTasks($tasks)
    {
        foreach ($tasks as $k => $task) {

            $this->_curl = new $this->_curlClass;

            if (!empty($task['options'])) {

                $this->_curl->setOptions($task['options']);
            }

            if (empty($task['url'])) {

                throw new Exception('Не указана ссылка на ресурс.');
            }

            $this->_curl->setUrl($task['url']);

            $task['ch'] = $this->_curl->getCh();
            $this->_tasks[$k] = $task;

            curl_multi_add_handle($this->_cmh, $this->_curl->getCh());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->_tasks;
    }

    /**
     * @return mixed
     */
    public function cleanTasks()
    {
        $this->_tasks = NULL;

        return $this;
    }

    /**
     * @return bool
     */
    public function multiGet()
    {
        if (empty($this->_tasks)) {

            return FALSE;
        }

        $active = NULL;
        do {

            $mrc = curl_multi_exec($this->_cmh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && ($mrc == CURLM_OK)) {

            if (curl_multi_select($this->_cmh) === -1) {
                usleep(100);
            }
            do {
                $mrc = curl_multi_exec($this->_cmh, $active);
                $info = curl_multi_info_read($this->_cmh);
                if ($info['msg'] == CURLMSG_DONE) {

                    $ch = $info['handle'];

                    $content = '';
                    if ($this->_getContent !== FALSE) {

                        $content = curl_multi_getcontent($ch);
                    }

                    foreach ($this->_tasks as &$t) {
                        if ($t['ch'] == $ch) {

                            $t['content'] = $content;

                            $t['info'] = [];
                            if ($this->_getInfo !== FALSE) {

                                $t['info'] = curl_getinfo($ch);
                            }
                        }
                    }
                    curl_multi_remove_handle($this->_cmh, $ch);
                    curl_close($ch);
                }
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
        curl_multi_close($this->_cmh);

        return $this->_tasks;
    }
}